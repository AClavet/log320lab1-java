import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Huffman {

	public static void main(final String[] args) {

		// String mode = "compresser";
		// String src = "data.txt";
		// String des = "data.huff";

		// String mode = "decompresser";
		final String src = "data.txt";
		final String des = "data.huf";

		final Huffman huffman = new Huffman(src, des);
		// huffman.run(mode);
		huffman.test();
	}

	static String pad(final String s, final int numDigits) {
		final StringBuffer sb = new StringBuffer(s);
		int numZeros = numDigits - s.length();
		while (numZeros-- > 0) {
			sb.insert(0, "0");
		}
		return sb.toString();
	}

	private String des = "";

	private String[] huffmanCodes = new String[256];

	Node n = null;

	private String src = "";

	public Huffman(final String src, final String des) {
		this.src = src;
		this.des = des;
	}

	private Boolean compress() {
		// log("\nCompressing...");
		final int[] data = readFile(src);
		final int[] frequencies = getFrequencies(data);

		final Node headNode = createTree(frequencies);
		getHoffmanCodes(headNode, new StringBuffer());

		return writeArchive(data, huffmanCodes, headNode);
	}

	private Boolean decompress() {
		// log("\nDecompressing...");
		final int[] data = readArchive(src);
		getHoffmanCodes(n, new StringBuffer());

		return decompressArchive(data);
	}

	private Node createTree(final int[] freq) {
		final PriorityQueue<Node> nodes = new PriorityQueue<Node>(freq.length,
				new Node());

		for (int i = 0; i < freq.length; i++) {
			if (freq[i] > 0) {
				nodes.offer(new Node(i, freq[i]));
			}
		}

		while (nodes.size() > 1) {
			final Node node = new Node();
			node.left = nodes.poll();
			node.right = nodes.poll();
			node.freq = node.left.freq + node.right.freq;

			nodes.add(node);
		}
		return nodes.poll();
	}

	private Boolean decompressArchive(final int[] data) {
		// log("Reading bytes :");
		for (final int element : data) {
			logln(String.valueOf(element) + " ");
		}

		final StringBuffer encodedMsg = new StringBuffer();
		final StringBuffer originalMsg = new StringBuffer();

		for (int i = 0; i < data.length - 2; i++) {
			final String bytes = pad(Integer.toBinaryString(data[i]), 7);
			encodedMsg.append(bytes);
		}
		final int l = data[data.length - 1];
		encodedMsg
				.append(pad(Integer.toBinaryString(data[data.length - 2]), l));
		// log("\nEncoded Msg : \n" + encodedMsg);

		DataOutputStream out = null;
		try {
			out = new DataOutputStream(new BufferedOutputStream(
					new FileOutputStream(des)));

			// log("\nWriting to file :");

			int i = 0;
			String s = "";
			while (i < encodedMsg.length()) {
				s += encodedMsg.charAt(i++);
				for (int j = 0; j < huffmanCodes.length; j++) {
					final String code = huffmanCodes[j];
					if (s.equals(code)) {
						if (j >= 128) {
							j -= 256;
						}
						originalMsg.append(j);
						// logln(j + " ");
						out.write(j);
						s = "";
						break;
					}
				}
			}
			out.close();
			return true;
		} catch (final IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	private Node extractTree(final DataInputStream in) throws IOException {
		final int[] frequencies = new int[256];
		final int headerLength = in.readShort();
		for (int i = 2; i < headerLength; i += 2) {
			int symbol, freq;
			symbol = in.readByte() & 0xFF;
			freq = in.readInt();
			frequencies[symbol] = freq;
		}
		return createTree(frequencies);
	}

	private int[] getFrequencies(final int[] data) {
		final int[] frequencies = new int[256];
		for (int i = 0; i < data.length; i++) {
			frequencies[data[i]] += 1;
		}
		return frequencies;
	}

	private void getHoffmanCodes(final Node node, final StringBuffer prefix) {
		if (node.isLeaf()) {
			huffmanCodes[node.symbol] = prefix.toString();
			// log("HUFF CODE FOR " + node.symbol + ": "
			// + huffmanCodes[node.symbol]);
		} else {
			getHoffmanCodes(node.left, prefix.append('0'));
			prefix.deleteCharAt(prefix.length() - 1);
			getHoffmanCodes(node.right, prefix.append('1'));
			prefix.deleteCharAt(prefix.length() - 1);
		}
	}

	private void log(final String msg) {
		// System.out.println(msg);
	}

	private void logln(final String msg) {
		// System.out.print(msg);
	}

	private int[] readArchive(final String path) {
		try {
			final DataInputStream in = new DataInputStream(
					new BufferedInputStream(new FileInputStream(path)));
			final int[] bytes = new int[(int) (new File(path)).length()];
			n = extractTree(in);

			int data = 0;
			int i = 0;
			while ((data = in.read()) != -1) {
				bytes[i++] = data & 0xFF;
			}

			in.close();
			return Arrays.copyOfRange(bytes, 0, i);
		} catch (final FileNotFoundException fe) {
			log("FileNotFoundException : " + fe);
		} catch (final IOException ioe) {
			log("IOException : " + ioe);
		}
		return null;
	}

	private int[] readFile(final String path) {
		try {

			final DataInputStream in = new DataInputStream(
					new BufferedInputStream(new FileInputStream(path)));
			final int[] bytes = new int[(int) (new File(path)).length()];

			int i = 0;
			int data = 0;
			while ((data = in.read()) != -1) {
				bytes[i++] = data & 0xFF;
			}

			in.close();
			return Arrays.copyOfRange(bytes, 0, i);
		} catch (final FileNotFoundException fe) {
			log("FileNotFoundException : " + fe);
		} catch (final IOException ioe) {
			log("IOException : " + ioe);
		}
		return null;
	}

	public Boolean run(final String mode) {
		if ("compresser".compareToIgnoreCase(mode) == 0) {
			return compress();
		} else if ("decompresser".compareToIgnoreCase(mode) == 0) {
			return decompress();
		}
		return false;
	}

	public Boolean test() {
		compress();
		huffmanCodes = new String[256];
		src = des;
		des = "output";
		return decompress();
	}

	private Boolean writeArchive(final int[] data, final String[] huffmanCodes,
			final Node headNode) {
		DataOutputStream out = null;
		try {
			out = new DataOutputStream(new BufferedOutputStream(
					new FileOutputStream(des)));

			int numCodes = 0;
			for (final String huffmanCode : huffmanCodes) {
				if (huffmanCode != null) {
					numCodes += 1;
				}
			}

			final int headerLength = (numCodes * 2) + 1;
			out.writeShort(headerLength);
			// log("\nWriting bytes :");
			// logln(headerLength + " ");
			writeHeader(headNode, out);

			final StringBuffer encodedMsg = new StringBuffer();
			for (final int element : data) {
				encodedMsg.append(huffmanCodes[element]);
			}

			final int bitSetLength = 7;
			String s = "";
			for (int i = 0; i < encodedMsg.length(); i += bitSetLength) {
				int endIndex = i + bitSetLength;

				if (endIndex > encodedMsg.length()) {
					endIndex = encodedMsg.length();
				}
				s = encodedMsg.substring(i, endIndex);

				final byte c = Byte.parseByte(s, 2);

				// logln(c + " ");
				out.write(c);
			}
			out.write(s.length());

			// log("\nOriginal content : ");
			// for (int i : data)
			// logln(i + " ");
			// log("\nEncoded msg : \n" + encodedMsg);

			out.close();
			return true;
		} catch (final IOException e1) {
			e1.printStackTrace();
			return false;
		}
	}

	private void writeHeader(final Node node, final DataOutputStream out)
			throws IOException {
		if (node.isLeaf()) {
			// logln(String.valueOf(node.symbol) + " ");
			out.writeByte(node.symbol);
			// logln(String.valueOf(node.freq) + " ");
			out.writeInt(node.freq);
			return;
		}
		writeHeader(node.left, out);
		writeHeader(node.right, out);
	}

	private class Node implements Comparator<Node> {
		public int freq;
		public Node left, right;
		public int symbol;

		Node() {
		}

		Node(final int symbol, final int freq) {
			this.symbol = symbol;
			this.freq = freq;
		}

		@Override
		public int compare(final Node n1, final Node n2) {
			if (n1.freq < n2.freq) {
				return -1;
			} else if (n1.freq > n2.freq) {
				return 1;
			}
			return 0;
		}

		private boolean isLeaf() {
			return ((left == null) && (right == null));
		}
	}

}